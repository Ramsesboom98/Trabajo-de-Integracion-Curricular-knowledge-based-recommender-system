# Trabajo-de-Integracion-Curricular-knowledge-based-recommender-system
Knowledge Based Recommender System

## Sistema de recomendación basado en el conocimiento


## Análisis v29 (resumen_v29.ipynb)

En el cuaderno "resumen_v29.ipynb" se realiza la extracción y el tratamiento de datos para la base de datos ChEMBL v29

Se tiene como resultado los csv:

- raw_activity_29
- activity_29
- summary29
- act_summary29

## Análisis v30 (resumen_v30.ipynb)

En el cuaderno "resumen_v30.ipynb" se realiza la extracción y el tratamiento de datos para la base de datos ChEMBL v30

Se tiene como resultado los csv:

- raw_activity_30
- summary30
- act_summary30


## Calculo intersecciones (resumen_intersecciones.ipynb)

Con el uso de los datos que se encuentran en "cell_c_radii_df.csv" y "cell_mapping.csv" junto con el valor de intersección entre células, se realiza el análisis de las relaciones entre células y se encuentra el top 10 de células que tienen un mayor valor de intersección. Este proceso se encuentra en el cuaderno "resumen_intersecciones.ipynb" y genera como resultado a los datos incluidos en "listas_top10_mapping.csv"

## Sistema de recomendación (prediccion_activos.py)

Previo a el uso del sistema de recomendación se realiza la creación de los datos de prueba "compounds30_ni_29.csv", este proceso se realiza en el cuaderno "preparacion_pruebas.ipynb".

El sistema de recomendación utiliza los siguientes datos:

- listas_top10_mapping.csv
- act_summary29.csv
- compounds30_ni_29.csv

El sistema de recomendación genera como resultado los siguientes conjuntos de datos 

- "recomendaciones_top10.csv":       Este conjunto de datos contiene las predicciones de actividades para las relaciones de compuestos y celulas existentes en los datos de prueba "compounds30_ni_29.csv".
- "recomendaciones_top10_ext.csv":   Este conjunto de datos contiene predicciones que se generaron a partir de las predicciones anteriores, es decir que estas no existen en los datos de prueba.

## Resultados (resultados.ipynb)

Los resultados y el análisis de los mismos se encuentran en el cuaderno resultados.ipynb, aqui se utiliza los datos "recomendaciones_top10.csv" y "compounds30_ni_29.csv" generando una matriz de confusión para evaluar las predicciones realizadas por el sistema de recomendación.
